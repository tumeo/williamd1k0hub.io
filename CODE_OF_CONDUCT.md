# Code of Conduct

>For a state that is not pleasant or delightful to me must be so to him also; and a state that is not pleasing or delightful to me, how could I inflict that upon another?

## Precepts

- Abstention from killing living beings
- Abstention from taking what is not given
- Abstention from sexual misconduct
- Abstention from false speech
- Abstention from intoxicating substances that cause inattention

## Virtues

- Kindness and compassion
- Generosity and renunciation
- Contentment and respect for faithfulness
- Being honest and dependable
- Mindfulness and responsibility
